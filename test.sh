#!/bin/sh
set -e
make clean all

convert image.png -resize "50%" half.png
./dcciscale half.png out.png

# it's not ideal but we have to shift the image to make a proper comparison
convert out.png -distort SRT "0,0 1 0 0.5,0.5" outc.png

octave test.m
magick compare -verbose -metric PSNR image.png outc.png /dev/null

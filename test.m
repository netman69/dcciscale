ORIG = imread("image.png");
OUT = imread("out.png");
b = 12;
[MSE, SNR, PSNR] = Calc_MSE_SNR(ORIG, OUT, b);
disp(['MSE = ', num2str(MSE)]);
disp(['SNR = ', num2str(SNR)]);
disp(['PSNR = ', num2str(PSNR)]);

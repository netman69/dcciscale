#include "img.h"

#include <stdio.h> /* fprintf() */
#include <math.h>

double clamp(double x, double lo, double hi) {
	return fmax(lo, fmin(hi, x));
}

/* ITU BT.601, but normalized. */
double luma(int px) {
	double r = (double) ((px >>  0) & 0xFF) / 255.0;
	double g = (double) ((px >>  8) & 0xFF) / 255.0;
	double b = (double) ((px >> 16) & 0xFF) / 255.0;
	return (r * 0.299 + g * 0.587 + b * 0.114) / (0.299 + 0.587 + 0.114);
}

int weightcolors(int *colors, double *weights, int num) {
	double r = 0, g = 0, b = 0;
	for (int i = 0; i < num; ++i) {
		r += (double) ((colors[i] >>  0) & 0xFF) * weights[i];
		g += (double) ((colors[i] >>  8) & 0xFF) * weights[i];
		b += (double) ((colors[i] >> 16) & 0xFF) * weights[i];
	}
	r = clamp(round(r), 0.0, 255.0);
	g = clamp(round(g), 0.0, 255.0);
	b = clamp(round(b), 0.0, 255.0);
	return (((int) r) << 0) | (((int) g) << 8) | (((int) b) << 16);
}

void dcci_pass1(img_t *img, int x, int y, double k, double T) {
	double d1 = 0, d2 = 0;

	int cNW = img_getpx(img, x - 1, y - 1);
	int cNE = img_getpx(img, x + 1, y - 1);
	int cSW = img_getpx(img, x - 1, y + 1);
	int cSE = img_getpx(img, x + 1, y + 1);
	int cNWW = img_getpx(img, x - 3, y - 1);
	int cNEE = img_getpx(img, x + 3, y - 1);
	int cSWW = img_getpx(img, x - 3, y + 1);
	int cSEE = img_getpx(img, x + 3, y + 1);
	int cNNW = img_getpx(img, x - 1, y - 3);
	int cNNE = img_getpx(img, x + 1, y - 3);
	int cSSW = img_getpx(img, x - 1, y + 3);
	int cSSE = img_getpx(img, x + 1, y + 3);
	int cNNWW = img_getpx(img, x - 3, y - 3);
	int cNNEE = img_getpx(img, x + 3, y - 3);
	int cSSWW = img_getpx(img, x - 3, y + 3);
	int cSSEE = img_getpx(img, x + 3, y + 3);

	/* 45 degree diagonal gradient. */
	d1 += fabs(luma(cNNW) - luma(cNWW));
	d1 += fabs(luma(cNNE) - luma(cNW)) +
	      fabs(luma(cNW) - luma(cSWW));
	d1 += fabs(luma(cNNEE) - luma(cNE)) +
	      fabs(luma(cNE) - luma(cSW)) +
	      fabs(luma(cSW) - luma(cSSWW));
	d1 += fabs(luma(cNEE) - luma(cSE)) +
	      fabs(luma(cSE) - luma(cSSW));
	d1 += fabs(luma(cSEE) - luma(cSSE));

	/* 135 degree diagonal gradient. */
	d2 += fabs(luma(cSWW) - luma(cSSW));
	d2 += fabs(luma(cNWW) - luma(cSW)) +
	      fabs(luma(cSW) - luma(cSSE));
	d2 += fabs(luma(cNNWW) - luma(cNW)) +
	      fabs(luma(cNW) - luma(cSE)) +
	      fabs(luma(cSE) - luma(cSSEE));
	d2 += fabs(luma(cNNW) - luma(cNE)) +
	      fabs(luma(cNE) - luma(cSEE));
	d2 += fabs(luma(cNNE) - luma(cNEE));

	double f[] = { -1.0 / 16.0, 9.0 / 16.0, 9.0 / 16.0, -1.0 / 16.0 }; // Catmull-Rom
	//double f[] = { -2 / 16.0, 10 / 16.0, 10 / 16.0, -2 / 16.0 };
	//double f[] = { -0.142857, 0.642857, 0.642857, -0.142857 }; // Lanczos-3
	int v1[] = { cNNEE, cNE, cSW, cSSWW };
	int v2[] = { cNNWW, cNW, cSE, cSSEE };
	int p;
	double dir = (1.0 + d1) / (1.0 + d2); /* Compute the directional index. */
	if (dir > T) {
		p = weightcolors(v2, f, 4);
	} else if (1.0 / dir > T) {
		p = weightcolors(v1, f, 4);
	} else {
		double wx = 1.0 / (1.0 + pow(d1, k)); /* Compute the weight vector. */
		double wy = 1.0 / (1.0 + pow(d2, k));
		double t = wx + wy;
		wx /= t;
		wy /= t;
		int p12[] = { v1[0], v1[1], v1[2], v1[3], v2[0], v2[1], v2[2], v2[3] };
		double w[] = {
			wx * f[0], wx * f[1], wx * f[2], wx * f[3],
			wy * f[0], wy * f[1], wy * f[2], wy * f[3]
		};
		p = weightcolors(p12, w, 8);
	}
	img_setpx(img, x, y, p);
}

void dcci_pass2(img_t *img, int x, int y, double k, double T) {
	double d1 = 0, d2 = 0;

	int cCW = img_getpx(img, x - 1, y    );
	int cCE = img_getpx(img, x + 1, y    );
	int cNC = img_getpx(img, x    , y - 1);
	int cSC = img_getpx(img, x    , y + 1);
	int cNWW = img_getpx(img, x - 2, y - 1);
	int cNEE = img_getpx(img, x + 2, y - 1);
	int cSWW = img_getpx(img, x - 2, y + 1);
	int cSEE = img_getpx(img, x + 2, y + 1);
	int cNNW = img_getpx(img, x - 1, y - 2);
	int cNNE = img_getpx(img, x + 1, y - 2);
	int cSSW = img_getpx(img, x - 1, y + 2);
	int cSSE = img_getpx(img, x + 1, y + 2);

	int cNNC = img_getpx(img, x    , y - 3);
	int cCWW = img_getpx(img, x - 3, y    );
	int cSSC = img_getpx(img, x    , y + 3);
	int cCEE = img_getpx(img, x + 3, y    );

	/* Horizontal gradient. */
	d1 += fabs(luma(cNWW) - luma(cSWW)) +
	      fabs(luma(cNC) - luma(cSC)) +
	      fabs(luma(cNEE) - luma(cSEE));
	d1 += fabs(luma(cNNW) - luma(cCW)) +
	      fabs(luma(cCW) - luma(cSSW));
	d1 += fabs(luma(cNNE) - luma(cCE)) +
	      fabs(luma(cCE) - luma(cSSE));

	/* These two extra terms are added in the Wikipedia description of tha algorithm and not the
	 *   paper or matlab implementation, they barely make a difference. They appear to make things
	 *   only worse when added.
	 */
	/*d1 += fabs(luma(cNC) - luma(cNNC));
	d1 += fabs(luma(cSSC) - luma(cSC));*/

	/* Vertical gradient. */
	d2 += fabs(luma(cNNW) - luma(cNNE)) +
	      fabs(luma(cCW) - luma(cCE)) +
	      fabs(luma(cSSW) - luma(cSSE));
	d2 += fabs(luma(cNWW) - luma(cNC)) +
	      fabs(luma(cNC) - luma(cNEE));
	d2 += fabs(luma(cSWW) - luma(cSC)) +
	      fabs(luma(cSC) - luma(cSEE));

	/* Also these are from the Wikipedia explanation. */
	/*d2 += fabs(luma(cCW) - luma(cCWW));
	d2 += fabs(luma(cCEE) - luma(cCE));*/

	double f[] = { -1.0 / 16.0, 9.0 / 16.0, 9.0 / 16.0, -1.0 / 16.0 }; // Catmull-Rom
	//double f[] = { -2 / 16.0, 10 / 16.0, 10 / 16.0, -2 / 16.0 };
	//double f[] = { -0.142857, 0.642857, 0.642857, -0.142857 }; // Lanczos-3
	int v1[] = { cNNC, cNC, cSC, cSSC };
	int v2[] = { cCWW, cCW, cCE, cCEE };
	int p;
	double dir = (1.0 + d1) / (1.0 + d2); /* Compute the directional index. */
	if (dir > T) {
		p = weightcolors(v2, f, 4);
	} else if (1.0 / dir > T) {
		p = weightcolors(v1, f, 4);
	} else {
		double wx = 1.0 / (1.0 + pow(d1, k)); /* Compute the weight vector. */
		double wy = 1.0 / (1.0 + pow(d2, k));
		double t = wx + wy;
		wx /= t;
		wy /= t;
		int p12[] = { v1[0], v1[1], v1[2], v1[3], v2[0], v2[1], v2[2], v2[3] };
		double w[] = {
			wx * f[0], wx * f[1], wx * f[2], wx * f[3],
			wy * f[0], wy * f[1], wy * f[2], wy * f[3]
		};
		p = weightcolors(p12, w, 8);
	}
	img_setpx(img, x, y, p);
}

void dcci(img_t *out, img_t *in, float k, float T) {
	/* The pixels we have get copied into the output, sparsely. */
	for (int x = 0; x < in->width; ++x) {
		for (int y = 0; y < in->height; ++y) {
			/* The latter 3 of the pixels set here only matter for the image's border. */
			img_setpx(out, x * 2, y * 2, img_getpx(in, x, y));
			img_setpx(out, x * 2 + 1, y * 2, img_getpx(in, x, y));
			img_setpx(out, x * 2, y * 2 + 1, img_getpx(in, x, y));
			img_setpx(out, x * 2 + 1, y * 2 + 1, img_getpx(in, x, y));
		}
	}

	/* Do the diagonal new pixels. */
	for (int x = 1; x < out->width; x += 2)
		for (int y = 1; y < out->height; y += 2)
			dcci_pass1(out, x, y, k, T);

	/* Do the remaining new pixels. */
	for (int x = 1; x < out->width; x += 2)
		for (int y = 0; y < out->height; y += 2)
			dcci_pass2(out, x, y, k, T);
	for (int x = 0; x < out->width; x += 2)
		for (int y = 1; y < out->height; y += 2)
			dcci_pass2(out, x, y, k, T);
}

int main(int argc, char *argv[]) {
	img_t input, output;
	if (argc != 3) {
		fprintf(stderr, "usage: %s <input file> <output file>\n", argv[0]);
		return 1;
	}
	if (img_read(&input, argv[1])) {
		fprintf(stderr, "failed to read input image\n");
		return 1;
	}
	if (img_create(&output, input.width * 2, input.height * 2)) {
		fprintf(stderr, "failed to allocate output image\n");
		return 1;
	}
	dcci(&output, &input, 5, 1.15);
	if (img_write(&output, argv[2])) {
		fprintf(stderr, "failed to write output image\n");
		return 1;
	}
	img_free(&input);
	return 0;
}

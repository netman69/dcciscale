#include "img.h"

#include <stdlib.h> /* malloc(), free() */
#include <string.h> /* memset() */
#include <stddef.h> /* NULL */

#define PNG_DEBUG 3
#include <png.h>

void img_free(img_t *img) {
	if (img->buf != NULL)
		free(img->buf);
	img->buf = NULL;
}

int img_read(img_t *img, char *file) {
	png_structp png_ptr;
	png_infop info_ptr;
	png_bytep *row_pointers;
	png_byte header[8];
	FILE *fp = fopen(file, "rb");
	if (fp == NULL)
		return 1;
	if (fread(header, 1, 8, fp) != 8 || png_sig_cmp(header, 0, 8))
		return 1;
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png_ptr == NULL) {
		fclose(fp);
		return 1;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		fclose(fp);
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		return 1;
	}
	if (setjmp(png_jmpbuf(png_ptr))) {
		fclose(fp);
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return 1;
	}
	png_init_io(png_ptr, fp);
	png_set_sig_bytes(png_ptr, 8);
	png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_STRIP_ALPHA |
			PNG_TRANSFORM_PACKING | PNG_TRANSFORM_PACKSWAP | PNG_TRANSFORM_EXPAND |
			PNG_TRANSFORM_SHIFT | PNG_TRANSFORM_GRAY_TO_RGB, NULL);
	row_pointers = png_get_rows(png_ptr, info_ptr);
	img->width = png_get_image_width(png_ptr, info_ptr);
	img->height = png_get_image_height(png_ptr, info_ptr);
	img->buf = malloc(img->width * img->height * 4);
	if (img->buf == NULL) {
		fclose(fp);
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return 1;
	}
	for (int x = 0; x < img->width; ++x) /* This isn't optimal, but it's easier. */
		for (int y = 0; y < img->height; ++y)
			img->buf[y * img->width + x] = (*(int *) (row_pointers[y] + x * 3)) & 0xFFFFFF;
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
	fclose(fp);
	return 0;
}

int img_create(img_t *img, int width, int height) {
	img->width = width;
	img->height = height;
	img->buf = malloc(width * height * 4);
	if (img->buf == NULL)
		return 1;
	memset(img->buf, 0, width * height * 4);
	return 0;
}

int img_write(img_t *img, char *file) {
	png_bytep *row_pointers;
	FILE *fp = fopen(file, "wb");
	if (fp == NULL)
		return 1;
	row_pointers = malloc(img->height * sizeof(png_bytep));
	if (row_pointers == NULL) {
		fclose(fp);
		return 1;
	}
	for (int i = 0; i < img->height; ++i)
		row_pointers[i] = (png_bytep) (img->buf + img->width * i);
	png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (png_ptr == NULL) {
		fclose(fp);
		return 1;
	}
	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		fclose(fp);
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		return 1;
	}
	if (setjmp(png_jmpbuf(png_ptr))) {
		fclose(fp);
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return 1;
	}
	png_init_io(png_ptr, fp);
	png_set_IHDR(png_ptr, info_ptr, img->width, img->height, 8, PNG_COLOR_TYPE_RGB,
			PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
	png_write_info(png_ptr, info_ptr);
	png_set_filler(png_ptr, 0, PNG_FILLER_AFTER);
	png_write_image(png_ptr, row_pointers);
	png_write_end(png_ptr, NULL);
	fclose(fp);
	free(row_pointers);
	return 0;
}

int img_getpx(img_t *img, int x, int y) {
	if (x < 0) x = 0;
	if (y < 0) y = 0;
	if (x >= img->width) x = img->width - 1;
	if (y >= img->height) y = img->height - 1;
	return img->buf[y * img->width + x];
}

void img_setpx(img_t *img, int x, int y, int px) {
	if (x < 0 || y < 0 || x >= img->width || y >= img->height)
		return;
	img->buf[y * img->width + x] = px;
}

#ifndef __IMG_H__
#define __IMG_H__

typedef struct {
	int width, height;
	int *buf;
} img_t;

extern void img_free(img_t *img);
extern int img_read(img_t *img, char *file);
extern int img_create(img_t *img, int width, int height);
extern int img_write(img_t *img, char *file);
extern int img_getpx(img_t *img, int x, int y);
extern void img_setpx(img_t *img, int x, int y, int px);

#endif /* __IMG_H__ */

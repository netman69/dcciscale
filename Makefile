CFLAGS  = -O2 -g -Wall -pedantic -std=c99
LDFLAGS = -lpng -lm
OBJS := img.o dcciscale.o

all: dcciscale

dcciscale: $(OBJS)

clean:
	rm -rf $(OBJS) dcciscale
